En esta iteración se pretende dotar al coche de un movimiento mas realista,
implementando las propiedades físicas expuestas en el
apartado~\ref{sec:fisicas-en-dinamica} de los antecedentes.

\subsection{Análisis}
\label{sec:analisis-4}

Implementar un componente de dinámica de vehículos que permita simular
el movimiento del coche de una forma mínimamente realista no es algo
trivial: se debe conseguir que el vehículo se vea afectado por las
ondulaciones del terreno, realizando el efecto que se consigue debido
a la amortiguación, así como distribuir dinámicamente el peso entre
las cuatro ruedas debido al efecto de aceleraciones y frenados, añadir
una fuerza de fricción a las ruedas que permita modelar la tracción de
éstas contra el terreno, etcétera.

Ésta última propiedad, la de fricción, es una de las que mas
complicadas de implementar.  Debido a las fuerzas que influyen en el
movimiento del coche, así como la distribución dinámica de carga, la
tracción de cada rueda varía considerablemente a lo largo del tiempo,
produciéndose lo que se conoce como derrapes. Sin embargo, debido a
que el objetivo que se persigue en este proyecto es el de desarrollar
un juego arcade, donde la conducción del vehículo no debe ser tan
realista, esta última propiedad no será implementada, sustituyéndola
por una modelo estático de fricción, donde dicha propiedad de los
neumáticos no varíe en el tiempo.

A la hora de implementar todas estas propiedades físicas, se ha optado
por no reinventar la rueda y utilizar una solución eficiente y bien
probada, el componente de dinámica de vehículos de Bullet.

Este componentede ofrece una implementación de vehículos basada en
rayqueries, de tal manera que se lanza un rayo por cada rueda del
coche. Usando como referencia el punto de contacto del rayo contra el
suelo, se calcula la longitud y la fuerza de la suspensión. La fuerza
de la suspensión se aplica sobre el chasis, de forma que no choque
contra el suelo. De hecho, el chasis del vehículo flota sobre el suelo
sustentándose sobre los rayos. La fuerza de fricción se calcula por
cada rueda que esté en contacto con el suelo. Esto se aplica como una
fuerza que permite simular el comportamiento que tendría un sistema de
suspensión.

Hay una serie de clases que son importantes a la hora de utilizar
vehículos en Bullet:
\begin{itemize}
\item \texttt{btRaycastVehicle}: Es la clase que modela el
  comportamiento del coche.
\item \texttt{btVehicleTuning}\label{def:btvehicleTuning}: clase que sirve como
  estructura de datos para el almacenamiento de los atributos del
  vehículo. Los mas importantes
  son: \begin{itemize} \item \texttt{btScalar m\_suspensionStiffness}:
  número en coma flotante que representa la rigidez (stiffness) de la
  suspensión. Se recomienda asignarle el valor de 10.0 para
  todoterrenos, 50.0 para coches deportivos y 200.0 para coches de
  formula 1.  \item \texttt{btScalar m\_suspensionCompression}:
  constante que representa un coeficiente de compresión de la
  suspensión.  \item \texttt{btScalar m\_suspensionDamping}:
  Coeficiente de \texttt{descompresión} de la amortiguación en el caso
  de que esté comprimida. Toma valores entre 0 y 1. El valor mínimo
  hace que la amortiguación rebote, mientras que el valor máximo hace
  que sea lo mas rígida posible. Entre 0.1 y 0.3 la amortiguación se
  suele comportar correctamente.  \item \texttt{btScalar
  m\_maxSuspensionTravelCm}: constante que representa la distancia
  máxima que puede ser comprimida la suspensión, expresada en
  centímetros.  \item \texttt{btScalar m\_frictionSlip}: El
  coeficiente de fricción entre el neumatico y el suelo. Para una
  simulación realista debería tomar el valor de 0.8; sin embargo, la
  conducción del vehículo mejora al aumentar el valor. Para coches de
  kart se aconseja asignarle valores muy altos (superiores a
  10000.0).  \item \texttt{btScalar m\_maxSuspensionForce}: constante
  que representa la fuerza máxima que podría ejercer la suspensión
  sobre el chasis. De esta constante depende el peso máximo del
  vehículo.  \end{itemize}
\end{itemize}

\subsection{Diseño e Implementación}
\label{sec:implementacion-4}

A continuación se van a explicar los detalles de la
Clase \texttt{Car}. En el listado~\ref{list:implementacion-car} se
puede ver la implementación de los métodos mas importantes, mientras
que en el listado~\ref{list:declaracion-car} se puede ver su
declaración:
\begin{itemize}
\item En el constructor se inicializa una serie de variables, así como un mapa que permite asociar acciones con un callback. Este mapa se usa en el método \texttt{exec()} para implementar el patrón \emph{Command}, aunque no se trata de una implementación totalmente fiel al patrón, ya que este patrón permite ejecutar una acción sin especificarla. En la figura~\ref{fig:command} se muestra la estructura ortodoxa del patrón.
\begin{figure}[h]
  \centering \includegraphics[width=0.8\textwidth]{Command.png} \caption[Patrón
  Command]{Patrón Command\protect\footnotemark} \label{fig:command}
\end{figure}
\footnotetext{\url{https://sourcemaking.com/design_patterns/command}}
\item Las acciones que puede realizar el coche son: acelerar, frenar, girar a la
  derecha y a la izquierda y usar Nitro.
\item La configuración del coche se realiza en el método \texttt{realize()}. En dicho método se crean el vehículo de bullet, las formas de colisión del coche, el cuerpo gráfico del mismo, se le indica la posición inicial, etcétera. Es necesario realizar la inicialización en el método \texttt{realize()} por varios motivos, el principal es que se necesita crear las instancias del vehículo antes de renderizarlo, de modo que resulta adecuado realizar el proceso de creación y el de inicialización de forma separada.
\item Dentro del proceso de inicialización, el vehículo se crea en varios pasos. En primer lugar, se crea el cuerpo rígido que se usará mas adelante en el método \texttt{init\_physic\_bodies()}. Tras esto se inicializa el vehículo en el método \texttt{init\_raycast\_car(physics)}. Este método recibe una referencia del gestor de físicas (clase \texttt{Physics}) y se crea una instancia de la clase \texttt{btDefaultVehicleRaycaster} (que es una interfaz para la creación de los rayos que harán de suspensión en el vehículo) así como de la clase \texttt{btRaycastVehicle} (clase que modela el comportamiento del vehículo). Por último se añade el vehículo recien creado al mundo físico mediante el método \texttt{dynamics\_world\_->addVehicle(vehicle\_)}, lo que lo añadirá a la simulación física.
\item Una vez que se ha creado el coche, se añaden las ruedas. Las ruedas son en realidad rayos con una longitud, determinada por la amortiguación, que utiliza \emph{Bullet} para calcular el punto de contacto, así como otros aspectos como la fuerza necesaria para mantener al vehículo suspendido sobre el suelo, aplicar torque, etcétera.
\item El último paso consiste en configurar el controlador del coche.
\end{itemize}
\begin{listing}[language=C++,
    caption = {Declaración de la parte pública de la clase Car},
    label={list:declaracion-car}]
class Car {
 public:
  typedef std::shared_ptr<Car> shared;
  CarController::shared controller_;
  (...)
  Ogre::SceneNode* chassis_node_;
  btRigidBody* chassis_body_;
  btRaycastVehicle::btVehicleTuning       tuning_;
  btVehicleRaycaster*                     vehicle_raycaster_;
  btRaycastVehicle*                       vehicle_;

  Car(std::string nick);
  virtual ~Car();
  (...)
  void realize(Physics::shared physics, Sound::shared sound,
               std::string name, btVector3 position, Ice::Byte id);

  void exec(Tinman::Action action);
  void accelerate();
  void use_nitro();
  void stop_accelerating();
  void brake();
  void stop_braking();
  void turn(Direction direction);
  void turn_right();
  void turn_left();
  void stop_turning();

  void update(float delta);
  (...)
};
 \end{listing}

\begin{listing}[language=C++,
    caption = {Implementación de algunos de los métodos mas importantes de la clase Car},
    label={list:implementacion-car}]
Car::Car(std::string nick) {
  nick_ = nick;
  accelerating_ = false;
  controller_ = std::make_shared<CarController>();
  lap_ = 0;
  current_segment_ = Section{};

  action_hooks[Tinman::Action::AccelerateBegin] =
            std::bind(&Car::accelerate, this);
  action_hooks[Tinman::Action::AccelerateEnd] =
            std::bind(&Car::stop_accelerating, this);
  action_hooks[Tinman::Action::BrakeBegin] =
            std::bind(&Car::brake, this);
  action_hooks[Tinman::Action::BrakeEnd] =
            std::bind(&Car::stop_braking, this);
  action_hooks[Tinman::Action::TurnRight] =
            std::bind(&Car::turn_right, this);
  action_hooks[Tinman::Action::TurnLeft] =
            std::bind(&Car::turn_left, this);
  action_hooks[Tinman::Action::TurnEnd] =
            std::bind(&Car::stop_turning, this);
  action_hooks[Tinman::Action::UseNitro] =
            std::bind(&Car::use_nitro, this);

  current_segment_ = Section{};
  sections_covered_ = 0;
  colliding_ = stuck_ = can_collide_ = false;
  colliding_time_ = invert_direction_ = not_moving_ = 0.f;
  std::cout << "[Car] constructor" << std::endl;
}

void
Car::exec(Tinman::Action action) {
  action_hooks[action]();
}

Car::realize(Physics::shared physics, Sound::shared sound, std::string name,
             btVector3 position, Ice::Byte id) {
  physics_ = physics;
  sound_ = sound;
  id_ = id;

  init_physic_bodies(physics, position);
  init_raycast_car(physics);

  add_wheels();
  configure_wheels();
  controller_->realize( wheels_nodes_);
}

void
Car::init_physic_bodies(Physics::shared physics, btVector3 position) {
  btTransform tr;
  tr.setIdentity();

  btVector3 origin = btVector3(0, 1, 0);
  compound_ = physics->
    create_compound_shape(origin, physics->create_shape(controller_->car_dimensions_));
  collision_shapes_.push_back(compound_);

  initial_position_ = position;
  chassis_body_ =  physics->
    create_rigid_body(btTransform( initial_rotation_,position),
                      chassis_node_, compound_, controller_->mass_);
  chassis_body_->setDamping(0.2,0.2);
  chassis_body_->setActivationState(DISABLE_DEACTIVATION);
}

void
Car::init_raycast_car(Physics::shared physics) {
  vehicle_raycaster_ = new btDefaultVehicleRaycaster(physics->dynamics_world_);
  vehicle_ = new btRaycastVehicle(tuning_ , chassis_body_, vehicle_raycaster_);

  physics->dynamics_world_->addVehicle(vehicle_);
}

void
Car::add_physic_wheel(bool is_front, btVector3 connection_point, int wheel_index) {
  vehicle_->addWheel(connection_point, controller_->wheel_direction_cs0_,
                     controller_->wheel_axle_cs_, controller_->suspension_rest_length_,
                     controller_->wheel_radius_,
                     tuning_ , is_front);
  wheels_nodes_[wheel_index]->translate(connection_point.getX() ,
                                        -0.3,
                                        connection_point.getZ());
}

void
Car::add_wheels() {
  const btScalar lateral_correction = (0.3 * controller_->wheel_width_);
  btScalar left_wheel = controller_->car_dimensions_.getX() - lateral_correction;
  btScalar right_wheel = -controller_->car_dimensions_.getX() + lateral_correction;
  btScalar front_wheel = controller_->car_dimensions_.getZ() - controller_->wheel_radius_;
  btScalar rear_wheel = -controller_->car_dimensions_.getZ() + controller_->wheel_radius_;

  add_physic_wheel(true, btVector3(left_wheel,   controller_->connection_height_,
                                   front_wheel),0);
  add_physic_wheel(true, btVector3(right_wheel,  controller_->connection_height_,
                                   front_wheel), 1);
  add_physic_wheel(false, btVector3(right_wheel, controller_->connection_height_,
                                    rear_wheel), 2);
  add_physic_wheel(false, btVector3(left_wheel,  controller_->connection_height_,
                                    rear_wheel), 3);
}



void
Car::configure_wheels(){
  for (int i=0; i < vehicle_->getNumWheels();i++) {
    btWheelInfo& wheel = vehicle_->getWheelInfo(i);
    wheel.m_suspensionStiffness = controller_->suspension_stiffness_;
    wheel.m_wheelsDampingRelaxation = controller_->suspension_damping_;
    wheel.m_wheelsDampingCompression = controller_->suspension_compression_;
    wheel.m_frictionSlip = controller_->wheel_friction_;
    wheel.m_rollInfluence = controller_->roll_influence_;
  }
}

//Hay muchas funciones que modelan el movimiento.
//Aqui solo se muestran las de aceleracion
void
Car::accelerate() {
  accelerating_ = true;
  controller_->accelerate();
}

void
Car::stop_accelerating() {
  accelerating_ = false;
  controller_->stop_accelerating();
}

(...)

void
Car::update(float delta) {
  update_lap();
  for (int wheel = 0; i < 4; ++i)
    vehicle_->applyEngineForce(controller_->f_engine_, i);

  for (int wheel = 0; i < 2; ++i)
    vehicle_->setSteeringValue(controller_->steering_, i);
}
\end{listing}

\begin{listing}[language=C++,
    caption = {Declaración de la clase CarController},
    label={list:declaracion-car-controller}]
    enum class Direction {right, left};
class CarController {
  float     f_max_engine_;
  float     acceleration_;
  float     deceleration_;
  float     f_max_braking_;
  float     steering_increment_;
  float     steering_clamp_;

  std::vector<Ogre::SceneNode*> wheels_nodes_;


 public:
  const           int rightIndex = 0;
  const           int upIndex = 1;
  const           int forwardIndex = 2;

  float     mass_;
  float     gvehicle_steering_;
  float     wheel_radius_;
  float     wheel_width_;
  float     suspension_stiffness_;
  float     wheel_friction_;
  float     suspension_damping_;
  float     suspension_compression_;
  float     roll_influence_          ;
  float     connection_height_;

  const btVector3 car_dimensions_ = btVector3(1, 0.5f, 2);
  const btScalar  suspension_rest_length_ = btScalar(0.5);
  const btVector3 wheel_direction_cs0_ = btVector3(0,-1,0);
  const btVector3 wheel_axle_cs_ = btVector3(-1,0,0);

  bool inverted_;

  typedef std::shared_ptr<CarController> shared;

  int nitro_;

  float f_engine_, f_braking_, steering_;
  bool  accelerating_, braking_, turning_;

  CarController();
  virtual ~CarController();
  void realize( std::vector<Ogre::SceneNode*> wheels_nodes);

  void accelerate();
  void stop_accelerating();

  void brake();
  void stop_braking();

  void turn( Direction direction);
  void stop_turning();

  void control_speed();

  virtual void update( float deltaT);

  void use_nitro();
};
\end{listing}


\begin{listing}[language=C++,
    caption = {Implementación de algunos de los métodos mas
    importantes de la clase CarController},
    label={list:implementacion-car-controller}]
    void
CarController::load_parameters(std::string file) {
  Parser parser;
  std::string pattern = R"(\w*\s*:\s*(-?\b\d*.\d*))";
  parser.inject(pattern);

  parser.open(file);
  Parser::Results parameters = parser.get_results();
  int index = 0;

  f_max_engine_               = str_to_float(parameters[index++][1]);
  acceleration_               = str_to_float(parameters[index++][1]);
  deceleration_               = str_to_float(parameters[index++][1]);
  f_max_braking_              = str_to_float(parameters[index++][1]);
  steering_increment_         = str_to_float(parameters[index++][1]);
  steering_clamp_             = str_to_float(parameters[index++][1]);
  mass_                       = str_to_float(parameters[index++][1]);
  gvehicle_steering_          = str_to_float(parameters[index++][1]);
  wheel_radius_               = str_to_float(parameters[index++][1]);
  wheel_width_                = str_to_float(parameters[index++][1]);
  suspension_stiffness_       = str_to_float(parameters[index++][1]);
  wheel_friction_             = str_to_float(parameters[index++][1]);
  suspension_damping_         = str_to_float(parameters[index++][1]);
  suspension_compression_     = str_to_float(parameters[index++][1]);
  roll_influence_             = str_to_float(parameters[index++][1]);
  connection_height_          = str_to_float(parameters[index++][1]);
}

void
CarController::accelerate() {
  if(f_engine_ >=  f_max_engine_) {
    accelerating_ = false;
    return;
  }
  f_engine_ += acceleration_;
  accelerating_ = true;
}

void
CarController::stop_accelerating() {
  accelerating_ = false;
  f_braking_ = deceleration_;
}
\end{listing}

En cuanto a la clase \texttt{CarController}, en los
listados~\ref{list:implementacion-car-controller}
y\ref{list:declaracion-car-controller} se muestran los métodos mas
importantes:
\begin{itemize}
\item en el método \texttt{load\_parameters(std::string file)} se lee un fichero de configuración que contiene los valores de las variables que mas afectan a la movilidad del coche. Un ejemplo del contenido de dicho fichero se puede ver en el listado~\ref{list:car.config}. Sacando a un fichero de texto los valores de los parámetros de configuración del coche se evita tener que compilar el proyecto cada vez que se cambia alguno de esos valores. Dado que una configuración podrá ser válida o inválida dependiendo de los deseos del equipo de desarrollo, en este caso la búsqueda de la configuración correcta se debe hacer mediante prueba y error. Hay que tener en cuenta que el tiempo de compilación de este proyecto ronda el minuto, de forma que con este mecanismo se ahorra una gran cantidad de tiempo.
\item Por otra parte, en esta clase se implementan las clases que modelan el movimiento del vehículo, la forma en que este acelera y frena, etcétera. Se ha creado una serie de métodos <<gemelos>>: unos que se ejecutan cuando se detecta una pulsación de una tecla y otros, cuyo nombre comienza en \texttt{stop\_}, cuando se detecta la liberación de la misma.

Para el ejemplo que se está mostrando aquí, el
método \texttt{accelerate} se ejecutará cuando se detecte la pulsación
de la tecla \texttt{W}, mientras que el
método \texttt{stop\_accelerate} cuando se libere dicha tecla. La
asociación de teclas-acción se realiza en la
clase \texttt{HumanController}, que se mencionará en la
sección~\ref{sec:implementacion-8}.
\end{itemize}

El flujo de ejecución de las acciones del coche es el siguiente:
\begin{itemize}
\item La clase \texttt{Car} recibe una invocación al método \texttt{exec()} con una acción determinada  pasada por argumento.
\item Dicho método invoca al apropiado de la clase \texttt{Car}, que a su vez invocará al de la clase \texttt{CarController}.
\end{itemize}

La clase \texttt{CarController} permite desacoplar la lógica de
control de la clase \texttt{Car}, de forma que podría haber diversos
tipos de controladores para el coche.

Explicado el proceso de inicialización y configuración del cocheq,
sólo queda mostrar las operaciones básicas de la
clase \texttt{btVehicleRaycaster}. La interfaz que ofrece Bullet para
controlar un vehículo es la siguiente:

\begin{itemize}
\item \texttt{void btRaycastVehicle::applyEngineForce( btScalar force, int
  wheel)}: aplica par motor a la rueda con ese índice. Los valores
  están expresados en N.m.
\item \texttt{void btRaycastVehicle::setBrake( btScalar brake, int
  wheelIndex)}: aplica frenado a la ruedas con ese índice.
\item \texttt{void btRaycastVehicle::setSteeringValue (btScalar steering, int
  wheel)}: gira la rueda con ese índice los grados que indica el primer
  argumento, expresados en radianes.
\end{itemize}

\begin{listing}[language=C++,
    caption = {Fichero de configuración del coche},
    label={list:car.config}]
f_max_engine_ : 5000.0
wheel_radius_ : 0.5
acceleration_ :  500.0
wheel_width_ : 0.5
deceleration_ : 200.0
suspension_stiffness_ : 200.0
f_max_braking_ : 400.0
wheel_friction_ : 10000.0
steering_increment_ : 0.05
suspension_damping_ : 2.0
steering_clamp_ :  0.25
suspension_compression_ : 0.1
mass_ : 900.0
roll_influence_ : 1.0
gvehicle_steering_ : 0.0
connection_height_ : 0.8
  \end{listing}

\begin{figure}[h]
  \centering
  \includegraphics[width=16cm]{carphysics.png}
  \caption[Resultado de la iteracion 4]{Resultado de la iteracion 4. Modelado mas realista del movimiento del coche y primera aproximación a creación de circuitos.}
  \label{fig:iteration4}
\end{figure}
