// Tinman author: Isaac Lacoba Molina
// Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef PHYSICS_H
#define PHYSICS_H
#include <OgreSceneNode.h>
#include <btBulletDynamicsCommon.h>

#include "motionstate.h"
#include "meshstrider.h"

float radians_to_angle(float radians);
float degree_to_radians(float degrees);

class Physics {
 public:
  typedef std::pair<const btCollisionObject*, const btCollisionObject*> CollisionPair;
  typedef std::map<CollisionPair, std::function<void()>> Triggers;
  typedef std::shared_ptr<Physics> shared;
  btDiscreteDynamicsWorld* dynamics_world_;

  const btScalar PI = 3.14159265359;

  Physics();
  virtual ~Physics();
  btRigidBody* create_rigid_body(const btTransform &world_transform,
                  Ogre::SceneNode* node,btCollisionShape* shape, btScalar mass);
  void add_rigid_body(btRigidBody* body_);
  btRigidBody* create_rigid_body(const btTransform &world_transform,
                                 btCollisionShape* shape, btScalar mass);
  void remove_rigid_body(btRigidBody* body);

  btCollisionShape* create_shape(btVector3 halfExtent);
  btCollisionShape* create_shape(float radius);
  btCollisionShape* create_shape(MeshStrider* strider);
  btCollisionShape* create_shape(btVector3 coordinates, btScalar distance_to_origin);
  btCompoundShape* create_compound_shape(btVector3 origin, btCollisionShape* child);

  void step_simulation(float deltaT, int maxSubSteps);

  void load_mesh(std::string file);
  void check_collision();

  void clear_triggers();
  void add_collision_hooks(CollisionPair key, std::function<void()> callback);

  void set_position(btRigidBody* body, btVector3 new_position);
  void set_transform(btRigidBody* body, btTransform transform);


  btCollisionWorld::ClosestRayResultCallback raytest(btVector3 position,
                                                     btVector3 next_destination);
  void print_vector(std::string message, btVector3 vector3);
  btVector3 rotate_vector(btVector3 vector, btVector3 axis, btScalar angle);

  btScalar get_angle(btVector3 origin, btVector3 destiny);

 private:
  const btVector3 gravity_ = btVector3(0, -40, 0);

  btBroadphaseInterface* broadphase_;
  btSequentialImpulseConstraintSolver* solver_;
  btDefaultCollisionConfiguration* collision_configuration_;
  btCollisionDispatcher* dispatcher_;

  Triggers triggers_;
};

#endif
