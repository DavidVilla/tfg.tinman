#ifndef CARFACTORY_H
#define CARFACTORY_H
#include "instrumentatedCarI.h"

class CarFactory {
  bool instrumentated_;
  Ice::ObjectAdapterPtr adapter_;

public:
  typedef std::shared_ptr<CarFactory> shared;

  CarFactory();
  CarFactory(Ice::ObjectAdapterPtr adapter);
  virtual ~CarFactory();

  Car::shared create(std::string nick);
};

#endif
