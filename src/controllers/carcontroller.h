// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CARCONTROLLER_H
#define CARCONTROLLER_H
#include <OgreSceneNode.h>

#include "parser.h"
#include "powerup.h"

enum class Direction {right, left};

class CarController {
  float     f_max_engine_;
  float     acceleration_;
  float     deceleration_;
  float     f_max_braking_;
  float     steering_increment_;
  float     steering_clamp_;

  std::vector<Ogre::SceneNode*> wheels_nodes_;


 public:
  const           int rightIndex = 0;
  const           int upIndex = 1;
  const           int forwardIndex = 2;

  float     mass_;
  float     gvehicle_steering_;
  float     wheel_radius_;
  float     wheel_width_;
  float     suspension_stiffness_;
  float     wheel_friction_;
  float     suspension_damping_;
  float     suspension_compression_;
  float     roll_influence_          ;
  float     connection_height_;

  const btVector3 car_dimensions_ = btVector3(1, 0.5f, 2);
  const btScalar  suspension_rest_length_ = btScalar(0.5);
  const btVector3 wheel_direction_cs0_ = btVector3(0,-1,0);
  const btVector3 wheel_axle_cs_ = btVector3(-1,0,0);

  bool inverted_;

  typedef std::shared_ptr<CarController> shared;

  int nitro_;

  float f_engine_, f_braking_, steering_;
  bool  accelerating_, braking_, turning_;

  CarController();
  virtual ~CarController();
  void realize( std::vector<Ogre::SceneNode*> wheels_nodes);

  void accelerate();
  void stop_accelerating();

  void brake();
  void stop_braking();

  void turn( Direction direction);
  void stop_turning();

  void control_speed();

  virtual void update( float deltaT);

  void use_nitro();

 private:
  void load_parameters(std::string file);
  void update_lap(std::vector<Ogre::Vector3> checkpoints);

  void turn_wheels(Direction direction);
  void reset_wheels();

  bool equals(float sut, float target, float delta) ;
  float str_to_float(std::string string);
};
#endif
