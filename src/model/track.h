// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef TRACK_H
#define TRACK_H
#include <iostream>
#include <fstream>
#include <string>
#include <stack>
#include <map>
#include <functional>
#include <uchar.h>

#include "physics.h"
#include "scene.h"
#include "animation.h"
#include "meshstrider.h"

struct Section {
  enum Type{Meta, Rect1, Rect2, RightCurve1,
            RightCurve2, LeftCurve1, LeftCurve2};
  Type type;
  Ogre::SceneNode* node;
  Ogre::Entity* physics_entity;
  btRigidBody* body;
  btVector3 position;
  int id;
  std::pair<char, char> direction;
};


class Track {
  bool init_;
  std::string file_name_;
	std::wifstream file_;
	std::map<wchar_t, std::function<Section()>> builder_;
  std::map<wchar_t, std::pair<char, char>> directions_;
  std::map<char, char> ends_;

  int x_, z_, index_;
  Scene::shared scene_;
  Physics::shared physics_;
  wchar_t current_char_;
  std::vector<Ogre::SceneNode*> stairs_;
public:
  typedef std::shared_ptr<Track> shared;
  std::vector<Section> segments_;
  int columns_;

  Track(std::string file);

  void next(std::string file);
  void next(Scene::shared scene, Physics::shared physics);
  Section create_section(std::string name);

  void reset();
 private:
  void open_file(std::string file);

  Ogre::SceneNode* create_graphic_element(std::string name);
  Section create_physic_element(std::string name, Ogre::SceneNode* graphic_element);
  Section::Type get_section_type(std::string name);

  void order_track();
  int get_next_section(int index, char direction);
  char get_direction(Section::Type type, Section::Type previous, char last_direction);
  void add_stairs();

};
#endif
