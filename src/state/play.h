// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Tinman author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef PLAY_H
#define PLAY_H
#include "state.h"

#include "session.h"


class Play : public State {
  Ogre::SceneNode* animation_node_;
  Ogre::Entity* animation_entity_;

 public:
  typedef std::shared_ptr<Play> shared;

  Play(std::shared_ptr<Game> game);
  virtual ~Play();

  void enter();
  void update(float delta);

 private:
  void config_race();
  void add_hooks();
  void create_overlays();
  void create_car();
  void destroy_board();
  void update_overlays();

  //from: http://stackoverflow.com/a/16606128/4319445
  std::string to_string_with_precision(const float number, const int n);
};

#endif
