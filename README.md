# **Tinman** #
This repository contains Isaac Lacoba's final degree project.
The main goal is to develop a clone videogame from the classical arcade game Ironman Super off road that will serve as study case to see how far is possible to implement a serie of agile techniques.

Este repositorio contiene el Trabajo Fin de Grado de Isaac Lacoba Molina.
El objetivo principal es desarrollar un clon del clásico juego arcade  "Ironman super off road" que servirá como caso de estudio para ver hasta que punto es factible implementar en un proyecto como éste una serie de técnicas de testing automático.

El juego dispone de modo multijugador en red y contra la máquina.