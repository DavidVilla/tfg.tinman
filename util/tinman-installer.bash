#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-
#install Utils
sudo apt-get -y install pkg-config;

#install OIS
sudo apt-get -y install libois-1.3.0 libois-dev;

#install Ogre
sudo apt-get -y install libogre-1.8.0 libogre-1.8-dev;

#install Bullet
sudo apt-get -y install libbullet-dev libbullet2.82-dbg  libbullet-extras-dev;

#install
sudo apt-get -y install libboost1.55-all-dev 

#install SDL
sudo apt-get -y install libsdl1.2-dev libsdl-image1.2-dev libsdl-sound1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev

#install CEGUI
./cegui-installer.bash;
